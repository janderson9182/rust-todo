mod in_memory;
mod persistent;

fn main() {
    loop {
        println!("Please make a choice 1-2:");
        println!("1. In-Memory TODO List");
        println!("2. SQLite TODO List");

        let mut buffer = String::new();
        std::io::stdin().read_line(&mut buffer).unwrap();
        buffer = buffer.trim().to_string();

        if buffer == "1" {
            in_memory::run();
        } else if buffer == "2" {
            persistent::run();
        } else {
            continue;
        }
    }
}
