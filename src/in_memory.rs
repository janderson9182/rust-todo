pub fn run() {
    let mut todos: Vec<Todo> = vec![];

    loop {
        println!("Please make a choice 1-8:");
        println!("1. Add new todo item");
        println!("2. List all");
        println!("3. List incomplete");
        println!("4. List complete");
        println!("5. Complete an item");
        println!("6. Un-Complete an item");
        println!("7. Remove an item");
        println!("8. Edit title");

        let mut buffer = String::new();
        std::io::stdin().read_line(&mut buffer).unwrap();
        buffer = buffer.trim().to_string();
        if buffer == "1" {
            get_new_from_user(&mut todos);
        } else if buffer == "2" {
            print_list(&todos);
        } else if buffer == "3" {
            print_list_filtered(&todos, false);
            continue;
        } else if buffer == "4" {
            print_list_filtered(&todos, true);
            continue;
        } else if buffer == "5" {
            change_status(&mut todos, true);
        } else if buffer == "6" {
            change_status(&mut todos, false);
        } else if buffer == "7" {
            remove(&mut todos);
        } else if buffer == "8" {
            change_title(&mut todos);
        } else {
            continue;
        }
    }
}

#[derive(Clone)]
struct Todo {
    name: String,
    is_done: bool,
}

impl Todo {
    fn new(name: String, is_done: bool) -> Todo {
        Todo {
            name,
            is_done,
        }
    }
}

fn print_list_filtered(todos: &Vec<Todo>, is_done: bool) {
    let incomplete: Vec<Todo> = todos
        .into_iter()
        .filter(|todo| is_done == todo.is_done)
        .cloned()
        .collect();

    print_list(&incomplete);
}

fn print_list(todos: &Vec<Todo>) {
    println!();
    println!("TODO LIST:");
    println!("----------");
    for (index, todo) in todos.iter().enumerate() {
        let icon = if todo.is_done { "✅" } else { "❌" };
        println!("{} - {} {}", index + 1, icon, todo.name);
    }
}

fn get_new_from_user(todos: &mut Vec<Todo>) {
    println!("Please enter a title for the todo");
    let mut buffer = String::new();
    std::io::stdin().read_line(&mut buffer).unwrap();
    buffer = buffer.trim().to_string();
    todos.push(Todo::new(buffer, false));
}

fn change_status(todos: &mut Vec<Todo>, is_done: bool) {
    print_list(&todos);

    if is_done {
        println!("Please enter the number you wish to complete");
    } else {
        println!("Please enter the number you wish to un-complete");
    }

    let index = get_index();

    let result = todos.get_mut(index);

    match result {
        // We found one at that index
        Some(todo) => todo.is_done = is_done,
        // The index was not valid
        None => println!("⚠️ Please enter a valid number"),
    }
}

fn remove(todos: &mut Vec<Todo>) {
    print_list(&todos);
    println!("Please enter the number you wish to remove");

    let index = get_index();

    todos.remove(index);
}

fn get_index() -> usize {
    let mut buffer = String::new();
    std::io::stdin().read_line(&mut buffer).unwrap();
    let index = buffer
        .trim()
        .to_string()
        .parse::<usize>()
        .unwrap_or(0) - 1;
    index
}

fn change_title(todos: &mut Vec<Todo>) {
    print_list(&todos);
    println!("Please enter the number you wish to remove");

    let index = get_index();

    println!("Please enter the new title:");
    let mut buffer = String::new();
    std::io::stdin().read_line(&mut buffer).unwrap();
    let new_title = buffer
        .trim()
        .to_string();

    let result = todos.get_mut(index);

    match result {
        // We found one at that index
        Some(todo) => todo.name = new_title,
        // The index was not valid
        None => println!("⚠️ Please enter a valid number"),
    }
}
