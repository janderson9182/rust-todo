use sqlite;

pub fn run() {
    let connection = sqlite::open("todo.sqlite").unwrap();

    connection.execute(
        "
        CREATE TABLE IF NOT EXISTS todos (
            id INTEGER PRIMARY KEY,
            name TEXT,
            is_done INTEGER
        );
        "
    ).unwrap();

    connection.execute(
        "
        INSERT INTO todos
               (name, is_done)
        values ('Example', FALSE);
        "
    ).unwrap();
}
